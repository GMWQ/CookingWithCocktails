Cooking with Cocktails - 3rd year Group Project 

Fellow authors:
- Charlotte Lokko: https://github.com/charlottelokko
- Alexandru-Bogdan Cumpanici: https://github.com/BogdanAlexandru11
- Orrin Blake: https://github.com/orrinblake96
- Paul Doyle: https://github.com/PaulDoyle21
- Michael McLoughlin

Project Overview
- Website designed as two piece application seperated between a recipe database and a cocktail database.
- Marketable towards individuals learning how to cook or perhaps hosting dinner parties.

Languages in use:
- HTML
- CSS
- JavaScript
- JSON

Live version of site hosted at:
http://cookingwithcocktails.ga/

